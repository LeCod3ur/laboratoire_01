package com.example.exer4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button btnLogin = (Button) findViewById(R.id.idButtonLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtUserName = (TextView) findViewById(R.id.idTextUserName);
                TextView txtPassword = (TextView) findViewById(R.id.idTextPassword);
                String userName = txtUserName.getText().toString();
                String password = txtPassword.getText().toString();
                if(userName.equalsIgnoreCase("user") && password.equalsIgnoreCase("pass")){
                    LinearLayout layoutLogin = (LinearLayout)  findViewById(R.id.LayoutLogin);
                    LinearLayout layoutImage = (LinearLayout)  findViewById(R.id.idLayoutImage);
                    layoutLogin.setVisibility(View.GONE);
                    layoutImage.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}